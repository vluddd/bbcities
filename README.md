# Backbase iOS Test - Vlad Smoc #

*Implementation details*

The entire cities data set is loaded into memory at the application start. We trade-off a high application start time in order to gain a fast search/filter functionality.

Cities are represented by structs.  
We use value types/structs in order to use the stack memory for speeding up the search process (as opposed to reference types which are stored on the heap).

For the search algorithm we use the built in "filter" function.

*Performance*

Tested on iMac and iPad Air

* iMac: App start ~ 6s,  Filter search ~ 200ms

* iPad Air: App start ~ 50s, Filter search ~ 450ms