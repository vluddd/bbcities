//
//  CitiesUnitTests.swift
//  CitiesUnitTests
//
//  Created by Vlad Lucian Smoc on 02/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import XCTest
@testable import Cities

class CitiesUnitTests: XCTestCase {
    
    typealias this = CitiesUnitTests
    var searchResultsUpdated = false
    var searchResults: [City]? = nil {
        didSet {
            searchResultsUpdated = searchResults != nil
        }
    }
    static var dataStore: CitiesDataStore = CitiesDataStore()
    
    override func setUp() {
        super.setUp()
        if this.dataStore.cities == nil {
            let exp = expectation(description: "\(#function)\(#line)")
            print("\(#function) Initializing data store...")
            this.dataStore.initializeDataStore(success: {
                exp.fulfill()
            }, failure: {error in
                print("\(#function) \(exp) failed with error \(error)")
                exp.fulfill()
            })
            waitForExpectations(timeout: 120, handler: nil)
        }
    }
    
    
     // MARK: - Test cases
    
    func testDataStoreIntegrity() {
        XCTAssert(this.dataStore.cities != nil && this.dataStore.cities!.count > 0, "Error verifying data store integrity")
    }
    
    func testDataStoreFullQuerySearch() {
        let queries = generateFullSearchQueries(10)
        
        for query in queries {
            evaluateSearchResults(query: query, querySuccess: { results in
                XCTAssert(results.count > 0, "Expected at least one search result, found none")
                print("\n\(#function) Received \(results.count) results for full query \(query)")
                
                for city in results {
                    if(city.name.count == query.count) {
                        XCTAssert(city.name == query, "Full query \(query) does not match city name \(city.name)")
                    }
                    else {
                        XCTAssert(city.name.hasPrefix(query), "Full query \(query) does not match city name \(city.name)")
                    }
                }
            }, queryFailure: {
                XCTFail("Data store search timed out")
            })
        }
    }
    
    func testDataStorePrefixQuerySearch() {
        let queries = generatePrefixSearchQueries(10)
        
        for prefixQuery in queries {
            evaluateSearchResults(query: prefixQuery, querySuccess: { results in
                XCTAssert(results.count > 0, "Expected at least one search result, found none")
                print("\n\(#function) Received \(results.count) results for prefix query \(prefixQuery)")
                
                for city in results {
                    XCTAssert(city.name.hasPrefix(prefixQuery), "Prefix query does not match city name prefix")
                }
            }, queryFailure: {
                XCTFail("Data store search timed out")
            })
        }
    }
    
    func testDataStoreInvalidSearch() {
        let query = "You won't find me here"
        evaluateSearchResults(query: query, querySuccess: { results in
            XCTAssert(results.count == 0, "Invalid query returned \(results.count) results: \(results)")
        }, queryFailure: {
            XCTFail("Data store search timed out")
        })
    }
    
    func testDataStoreEmptySearch() {
        let query = ""
        evaluateSearchResults(query: query, querySuccess: { results in
            XCTAssert(results.count == this.dataStore.cities!.count, "Empty search results count different than data store cities count")
        }, queryFailure: {
            XCTFail("Data store search timed out")
        })
    }
    
    
    // MARK: - Helper methods
    
    func evaluateSearchResults(query: String, querySuccess: ([City]) -> (), queryFailure: () -> ()) {
        searchResults = nil
        
        this.dataStore.searchCity(query, success: { [unowned self] cities in
            self.searchResults = cities
        })
        
        let pred = NSPredicate(format: "searchResultsUpdated == true")
        let exp = expectation(for: pred, evaluatedWith: self, handler: nil)
        let res = XCTWaiter.wait(for: [exp], timeout: 5)
        
        if res == XCTWaiter.Result.completed {
            querySuccess(searchResults!)
        }
        else {
            queryFailure()
        }
    }
    
    func generateFullSearchQueries(_ numberOfItems: Int) -> [String] {
        var queries: [String] = []
        for _ in 1...numberOfItems {
            let index = Int(arc4random_uniform(UInt32(this.dataStore.cities!.count)))
            let query = this.dataStore.cities![index].name
            queries.append(query)
        }
        return queries
    }
    
    func generatePrefixSearchQueries(_ numberOfItems: Int) -> [String] {
        var prefixQueries: [String] = []
        let queries = generateFullSearchQueries(numberOfItems)
        for query in queries {
            prefixQueries.append(splitForPrefixQuery(query))
        }
        return prefixQueries
    }
    
    func splitForPrefixQuery(_ fullQuery: String) -> String {
        let splitIndex = Int(arc4random_uniform(UInt32(fullQuery.count)) + 1)
        var result: String = ""
        for (index, character) in fullQuery.enumerated() {
            if(index == splitIndex) {
                break
            }
            result.append(character)
        }
        return result
    }
}
