//
//  CitiesDataStore.swift
//  Cities
//
//  Created by Vlad Lucian Smoc on 03/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import Foundation


/*  The entire cities data set is stored into memory inside an array, with a city being represented by a struct
    We use value types/structs in order to use the stack memory for speeding up the search process (as opposed to reference types which are stored on the heap)
    For the search algorithm we use the built in "filter" function */

class CitiesDataStore {
    
    public func initializeDataStore(success: @escaping () -> Void, failure: @escaping (_ error: Error) -> Void) {
        JSONCityLoader.loadCities(success: { [weak self] cities in
            guard let strongSelf = self else { return }
            strongSelf.cities = strongSelf.sortCities(cities)
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    public func searchCity(_ query: String, success: @escaping (_ cities: [City]) -> Void) {
        currentQuery = query
        
        guard let cities = cities, cities.count > 0 else {
            success([])
            return
        }
        
        guard query.count > 0 else {
            success(cities)
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            
            guard let strongSelf = self else { return }
            let results = cities.filter({ city -> Bool in
                return city.name.hasPrefix(query)
            })
            
            // If the user started a subsequent query before this thread did finish executing, the results are outdated, we do not callback
            if strongSelf.currentQuery == query {
                DispatchQueue.main.async {
                    success(results)
                }
            }
        }
    }
    
    private func sortCities(_ cities: [City]) -> [City] {
        return cities.sorted(by: { first, second -> Bool in
            return first.name < second.name
        })
    }
    
    private(set) var cities: [City]? = nil
    private var currentQuery = CitiesDataStore.emptyQuery
    private static let emptyQuery = ""
}
