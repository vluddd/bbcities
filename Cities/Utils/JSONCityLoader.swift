//
//  JSONCityLoader.swift
//  Cities
//
//  Created by Vlad Lucian Smoc on 02/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import Foundation

struct JSONCityLoader {
    
    public static func loadCities(success: @escaping (_ cities: [City]) -> Void, failure: @escaping (_ error: Error) -> Void) {
        DispatchQueue.global(qos: .background).async {
            
            guard let path = Bundle.main.path(forResource: filename, ofType: fileExtension), let data = NSData.init(contentsOfFile: path) else {
                failure(generateFileReadError())
                return
            }
            
            do {
                let cities = try JSONDecoder().decode([City].self, from: data as Data)
                DispatchQueue.main.async {
                    success(cities)
                }
            }
            catch let jsonError {
                DispatchQueue.main.async {
                    failure(jsonError)
                }
            }
        }
    }
    
    private static func generateFileReadError() -> NSError {
        return NSError(domain:errorDomain, code:errorCode, userInfo:[ NSLocalizedDescriptionKey: errorDescription])
    }
    
    private static let filename = "cities"
    private static let fileExtension = "json"
    private static let errorDomain = "Load JSON"
    private static let errorCode = 1010
    private static let errorDescription = "Error reading data from local JSON file"
}
