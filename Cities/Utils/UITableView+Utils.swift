//
//  UITableView+Keyboard.swift
//  Cities
//
//  Created by Vlad Lucian Smoc on 03/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import UIKit

extension UITableView {
    
    func scrollToTop() {
        guard numberOfRows(inSection: 0) > 0 else { return }
        let indexPath = IndexPath(row: 0, section: 0)
        scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func subscribeToKeyboardResizing() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func unsubscribeFromKeyboardResizing() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let superview = self.superview else { return }
        
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = superview.convert(keyboardScreenEndFrame, from: superview.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            contentInset = UIEdgeInsets.zero
        } else {
            contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        scrollIndicatorInsets = contentInset
    }
}
