//
//  CityMapViewController.swift
//  Cities
//
//  Created by Vlad Lucian Smoc on 04/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import UIKit
import MapKit

class CityMapViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = city?.name
        displayCity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    private func displayCity() {
        guard let city = city else {
            return
        }
        
        let annotation = MKPointAnnotation()
        annotation.title = city.name
        annotation.coordinate = CLLocationCoordinate2D(latitude: city.coord.lat, longitude: city.coord.lon)
        mapView.addAnnotation(annotation)
        mapView.setCenter(CLLocationCoordinate2DMake(city.coord.lat, city.coord.lon), animated: false)
    }
    
    var city: City? = nil
    @IBOutlet weak var mapView: MKMapView!
}
