//
//  CitiesViewController.swift
//  Cities
//
//  Created by Vlad Lucian Smoc on 02/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Cities"
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == CitiesViewController.cityMapViewSegueId, let cityMapVC = segue.destination as? CityMapViewController , let city = sender as? City else { return }
        cityMapVC.city = city
    }
    
    deinit {
        tableView.unsubscribeFromKeyboardResizing()
    }
    
    private func loadData() {
        activityIndicator.startAnimating()
        tableView.subscribeToKeyboardResizing()
        searchBar.isUserInteractionEnabled = false
        
        dataStore.initializeDataStore(success: { [unowned self] in
            self.activityIndicator.stopAnimating()
            self.searchBar.isUserInteractionEnabled = true
            self.tableView.reloadData()
        }) { error in
            print("Error loading data store \(error)")
        }
    }
    
    private func resetSearchResults() {
        searchResults = []
    }
    
    internal let dataStore = CitiesDataStore()
    internal var searchResults: [City] = []
    internal var isFiltering = false {
        didSet {
            if !isFiltering {
                searchBar.text = ""
                resetSearchResults()
                tableView.reloadData()
                tableView.scrollToTop()
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    static internal let cityMapViewSegueId = "CityListToCityMapSegue"
}

extension CitiesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let cities = isFiltering ? searchResults : dataStore.cities else { return 0 }
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cities = isFiltering ? searchResults : dataStore.cities
        let cell = tableView.dequeueReusableCell(withIdentifier: CitiesViewController.cityCellId)
        cell!.textLabel!.text = cities![indexPath.row].name
        cell!.detailTextLabel!.text = cities![indexPath.row].country
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cities = isFiltering ? searchResults : dataStore.cities
        let city = cities![indexPath.row]
        performSegue(withIdentifier: CitiesViewController.cityMapViewSegueId, sender: city)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private static let cityCellId = "CityCell"
}

extension CitiesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard searchText.count > 0 else {
            isFiltering = false
            return
        }
        
        isFiltering = true
        activityIndicator.startAnimating()
        
        dataStore.searchCity(searchText) { [unowned self] cities in
            self.activityIndicator.stopAnimating()
            self.searchResults = cities
            self.tableView.reloadData()
            self.tableView.scrollToTop()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isFiltering = false
        searchBar.resignFirstResponder()
    }
}
