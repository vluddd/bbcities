//
//  City.swift
//  Cities
//
//  Created by Vlad Lucian Smoc on 02/03/2018.
//  Copyright © 2018 Vlad Smoc. All rights reserved.
//

import Foundation

struct Coordinate: Decodable {
    let lat: Double
    let lon: Double
}

struct City: Decodable {
    let name: String
    let country: String
    let _id: Int
    let coord: Coordinate
}
